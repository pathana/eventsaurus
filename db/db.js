var faker = require('faker');

module.exports = function() {
  var data = { slides: [], events: [], categories: {}, tickets: [], orders: [] }

  for (var i = 1; i <= 4; i++) {
    data.slides.push({ id: i, content: faker.lorem.paragraph(), image: faker.image.image() })
  }

  for (var i = 1; i <= 6; i++) {
    data.events.push({ id: i, subject: faker.lorem.words(),
      date_time: faker.date.recent(), place_address: faker.address.state()+" "+faker.address.zipCode(),
      image: faker.image.image()
    })
  }

  var all = []
  for (var i = 1; i <= 6; i++) {
    all.push({id: i, subject: faker.company.companyName(), date_time: faker.date.recent(),
      place_address: faker.address.state()+" "+faker.address.zipCode(), image: faker.image.image()
    })
  }

  var business = []
  for (var i = 1; i <= 6; i++) {
    business.push({id: i, subject: faker.company.companyName(), date_time: faker.date.recent(),
      place_address: faker.address.state()+" "+faker.address.zipCode(), image: faker.image.image()
    })
  }

  var others = []
  for (var i = 1; i <= 6; i++) {
    others.push({id: i, subject: faker.company.companyName(), date_time: faker.date.recent(),
      place_address: faker.address.state()+" "+faker.address.zipCode(), image: faker.image.image()
    })
  }

  data.categories['all'] = all
  data.categories['business'] = all
  data.categories['others'] = all

  for (var i = 1; i <= 4; i++) {
    var holders = []
    for (var j = 1; j <= 2; j++) {
      holder = {
                  id: j, name: faker.name.firstName()+" "+faker.name.lastName(), type: "Normal Ticket",
                  price: faker.random.number()+" THB", qr_code: "https://developer.android.com/samples/Notifications/Application/res/drawable-nodpi/qr_code.png"
               }
      holders.push(holder);
    }

    data.tickets.push({ id: i, subject: faker.lorem.words(),
      date_time: faker.date.future(), time: faker.date.weekday(),
      place_address: faker.address.state()+" "+faker.address.zipCode(), image: faker.image.image(), holders: holders
    })
  }

  for (var i = 1; i <= 4; i++) {
    data.orders.push({ id: i, subject: faker.lorem.words(),
      date_time: faker.date.future(), place_address: faker.address.state()+" "+faker.address.zipCode(),
      name: faker.name.firstName()+" "+faker.name.lastName(),
      email: faker.internet.email(), phone_number: faker.phone.phoneNumber(), total_price: "$"+faker.random.number(),
      payment_via: "Visa", paid_at: faker.date.past()
    })
  }
  return data
}
