'use strict';

describe('Component: UpcomingComponent', function() {
  // load the controller's module
  beforeEach(module('eventsaurusApp.upcoming'));

  var UpcomingComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    UpcomingComponent = $componentController('upcoming', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
