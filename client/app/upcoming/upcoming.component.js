'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './upcoming.routes';

export class UpcomingComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('eventsaurusApp.upcoming', [uiRouter])
  .config(routes)
  .component('upcoming', {
    template: require('./upcoming.html'),
    controller: UpcomingComponent,
    controllerAs: 'upcomingCtrl'
  })
  .name;
