'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('upcoming', {
      url: '/upcoming',
      template: '<upcoming></upcoming>'
    });
}
