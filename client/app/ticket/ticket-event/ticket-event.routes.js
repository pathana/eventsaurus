'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('ticket-event', {
      url: '/ticket/event',
      template: '<ticket-event></ticket-event>'
    });
}
