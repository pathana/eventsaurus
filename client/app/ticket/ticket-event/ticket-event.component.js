'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './ticket-event.routes';

export class TicketEventComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('eventsaurusApp.ticket-event', [uiRouter])
  .config(routes)
  .component('ticketEvent', {
    template: require('./ticket-event.html'),
    controller: TicketEventComponent,
    controllerAs: 'ticketEventCtrl'
  })
  .name;
