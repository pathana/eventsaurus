'use strict';

describe('Component: TicketEventComponent', function() {
  // load the controller's module
  beforeEach(module('eventsaurusApp.ticket-event'));

  var TicketEventComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    TicketEventComponent = $componentController('ticket-event', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
