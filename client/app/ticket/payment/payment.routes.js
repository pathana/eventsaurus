'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('payment', {
      url: '/ticket/payment',
      template: '<payment></payment>'
    });
}
