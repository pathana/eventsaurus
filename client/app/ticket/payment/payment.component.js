'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './payment.routes';

export class PaymentComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('eventsaurusApp.payment', [uiRouter])
  .config(routes)
  .component('payment', {
    template: require('./payment.html'),
    controller: PaymentComponent,
    controllerAs: 'paymentCtrl'
  })
  .name;
