'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('organizer', {
      url: '/ticket/organizer',
      template: '<organizer></organizer>'
    });
}
