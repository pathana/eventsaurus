'use strict';

describe('Component: OrganizerComponent', function() {
  // load the controller's module
  beforeEach(module('eventsaurusApp.organizer'));

  var OrganizerComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    OrganizerComponent = $componentController('organizer', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
