'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './organizer.routes';

export class OrganizerComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('eventsaurusApp.organizer', [uiRouter])
  .config(routes)
  .component('organizer', {
    template: require('./organizer.html'),
    controller: OrganizerComponent,
    controllerAs: 'organizerCtrl'
  })
  .name;
