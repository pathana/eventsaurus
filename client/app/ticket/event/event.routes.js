'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('event', {
      url: '/ticket/event',
      template: '<event></event>'
    });
}
