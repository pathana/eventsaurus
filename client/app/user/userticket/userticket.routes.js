'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('userticket', {
      url: '/user/ticket',
      template: '<userticket></userticket>'
    });
}
