'use strict';

describe('Component: UserticketComponent', function() {
  // load the controller's module
  beforeEach(module('eventsaurusApp.userticket'));

  var UserticketComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    UserticketComponent = $componentController('userticket', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
