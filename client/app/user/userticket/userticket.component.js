'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './userticket.routes';

export class UserticketComponent {
  /*@ngInject*/
  constructor($http, $scope) {
    this.$http = $http;
    this.scope = $scope;
  }

  getDataTicket(scope) {
   this.$http.get('http://127.0.0.1:3003/tickets')
     .then(response => {
        scope.tickets = response.data;
     });
  }

  getDataOrder(scope) {
   this.$http.get('http://127.0.0.1:3003/orders')
     .then(response => {
        scope.orders = response.data;
     });
  }

  $onInit() {
    var scope = this.scope;
    this.getDataTicket(scope);
    this.getDataOrder(scope);
  }
}

export default angular.module('eventsaurusApp.userticket', [uiRouter])
  .config(routes)
  .component('userticket', {
    template: require('./userticket.html'),
    controller: UserticketComponent,
    controllerAs: 'userticketCtrl'
  })
  .name;
