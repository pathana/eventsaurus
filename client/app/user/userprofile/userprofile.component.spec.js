'use strict';

describe('Component: UserprofileComponent', function() {
  // load the controller's module
  beforeEach(module('eventsaurusApp.userprofile'));

  var UserprofileComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    UserprofileComponent = $componentController('userprofile', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
