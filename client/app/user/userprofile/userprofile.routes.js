'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('userprofile', {
      url: '/user/profile',
      template: '<userprofile></userprofile>'
    });
}
