'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './userprofile.routes';

export class UserprofileComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('eventsaurusApp.userprofile', [uiRouter])
  .config(routes)
  .component('userprofile', {
    template: require('./userprofile.html'),
    controller: UserprofileComponent,
    controllerAs: 'userprofileCtrl'
  })
  .name;
