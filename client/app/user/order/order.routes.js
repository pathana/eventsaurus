'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('order', {
      url: '/user/order',
      template: '<order></order>'
    });
}
