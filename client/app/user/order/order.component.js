'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './order.routes';

export class OrderComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('eventsaurusApp.order', [uiRouter])
  .config(routes)
  .component('order', {
    template: require('./order.html'),
    controller: OrderComponent,
    controllerAs: 'orderCtrl'
  })
  .name;
