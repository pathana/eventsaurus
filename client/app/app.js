'use strict';

import angular from 'angular';
// import ngAnimate from 'angular-animate';
import ngCookies from 'angular-cookies';
import ngResource from 'angular-resource';
import ngSanitize from 'angular-sanitize';

import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import bootstrap from 'bootstrap';
// import ngMessages from 'angular-messages';


import {
  routeConfig
} from './app.config';

import navbar from '../components/navbar/navbar.component';
import navbarAuth from '../components/navbar-auth/navbar-auth.component';
import headerweb from '../components/headerweb/headerweb.component';
import upcomingEvent from
  '../components/upcoming-event/upcoming-event.component';
import category from '../components/category/category.component';
import individulprofile from
  './organizer/profile/components/individulprofile/individulprofile.component';
import companyProfile from
  './organizer/profile/components/companyProfile/companyProfile.component';
import performerProfile from
  './organizer/profile/components/performerProfile/performerProfile.component';
import performerExp from
  './organizer/profile/components/performerExp/performerExp.component';
import getApp from '../components/get-app/get-app.component';
import footerweb from '../components/footerweb/footerweb.component';
import main from './main/main.component';
import upcoming from './upcoming/upcoming.component';
import userProfile from './user/userprofile/userprofile.component';
import userTicket from './user/userticket/userticket.component';
import userOrder from './user/order/order.component';
import ticketEvent from './ticket/ticket-event/ticket-event.component';
import ticketPayment from './ticket/payment/payment.component';
import ticketOrganizer from './ticket/organizer/organizer.component';
import organizerProfile from './organizer/profile/profile.component';
import organizerEvent from './organizer/event/event.component';
import constants from './app.constants';
import util from '../components/util/util.module';
import fileinput from '../components/fileinput/fileinput.directive';
import upload from '../components/upload/upload.module';

import './app.scss';

angular.module('exampleApp',
  [ngCookies, ngResource, ngSanitize, uiRouter,
    uiBootstrap, navbar, headerweb, footerweb,
    main, upcoming, constants, util, fileinput,
    upcomingEvent, category, getApp, upload,
    userTicket, userOrder, userProfile,
    ticketEvent, ticketPayment, ticketOrganizer,
    organizerProfile, individulprofile,
    companyProfile, performerProfile, performerExp,
    organizerEvent, navbarAuth
  ])
  .config(routeConfig);

angular.element(document)
  .ready(() => {
    angular.bootstrap(document, ['exampleApp'], {
      strictDi: true
    });
  });
