import angular from 'angular';

export class PerformerexpComponent {}

export default angular.module('directives.performerexp', [])
  .component('performerexp', {
    template: require('./performerExp.html'),
    controller: PerformerexpComponent
  })
  .name;
