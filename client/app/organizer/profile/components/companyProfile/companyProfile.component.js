import angular from 'angular';

export class CompanyprofileComponent {}

export default angular.module('directives.companyprofile', [])
  .component('companyprofile', {
    template: require('./companyProfile.html'),
    controller: CompanyprofileComponent
  })
  .name;
