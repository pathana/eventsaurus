import angular from 'angular';

export class IndividulprofileComponent {}

export default angular.module('directives.individulprofile', [])
  .component('individulprofile', {
    template: require('./individulprofile.html'),
    controller: IndividulprofileComponent
  })
  .name;
