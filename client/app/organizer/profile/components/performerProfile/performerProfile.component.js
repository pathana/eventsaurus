import angular from 'angular';

export class PerformerprofileComponent {}

export default angular.module('directives.performerprofile', [])
  .component('performerprofile', {
    template: require('./performerProfile.html'),
    controller: PerformerprofileComponent
  })
  .name;
