'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('profile', {
      url: '/organizer/profile',
      template: '<profile></profile>'
    });
}
