'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('event', {
      url: '/organizer/event',
      template: '<event></event>'
    });
}
