import angular from 'angular';


export class NavbarAuthComponent {}

export default angular.module('directives.navbarAuth', [])
  .component('navbarAuth', {
    template: require('./navbar-auth.html'),
    controller: NavbarAuthComponent
  })
  .name;
