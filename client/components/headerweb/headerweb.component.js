import angular from 'angular';


export class HeaderWebComponent {
  /*@ngInject*/
  constructor($http, $scope) {
    this.$http = $http;
    this.scope = $scope;
  }

  getDataSlide(scope) {
   this.$http.get('http://127.0.0.1:3003/slides')
     .then(response => {
       scope.slides = response.data;
     });
  }

  $onInit() {
    var scope = this.scope;
    this.getDataSlide(scope);
  }
}

export default angular.module('directives.headerweb', [])
  .component('headerweb', {
    template: require('./headerweb.html'),
    controller: HeaderWebComponent
  })
  .name;
