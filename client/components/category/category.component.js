import angular from 'angular';

export class CategoryComponent {
  /*@ngInject*/
  constructor($http, $scope) {
    this.$http = $http;
    this.scope = $scope;
  }

  getDataCategory(scope) {
   this.$http.get('http://127.0.0.1:3003/categories')
     .then(response => {
        scope.categories = response.data;
     });
  }

  $onInit() {
    var scope = this.scope;
    this.getDataCategory(scope);
  }
}

export default angular.module('directives.category', [])
  .component('category', {
    template: require('./category.html'),
    controller: CategoryComponent
  })
  .name;
