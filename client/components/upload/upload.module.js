'use strict';

import angular from 'angular';
import {
  UploadService
} from './upload.service';

export default angular.module('exampleApp.uploadService', [])
  .service('uploadService', UploadService)
  .name;
