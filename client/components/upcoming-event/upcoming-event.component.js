import angular from 'angular';

export class UpcomingEventComponent {
  /*@ngInject*/
  constructor($http, $scope) {
    this.$http = $http;
    this.scope = $scope;
  }

  getDataEvents(scope) {
    this.$http.get('http://127.0.0.1:3003/events')
      .then(response => {
        scope.events = response.data;
      });
  }

  $onInit() {
    var scope = this.scope;
    this.getDataEvents(scope);
  }
}

export default angular.module('directives.upcomingEvent', [])
  .component('upcomingEvent', {
    template: require('./upcoming-event.html'),
    controller: UpcomingEventComponent
  })
  .name;
