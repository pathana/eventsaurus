import angular from 'angular';

export class FooterWebComponent {}

export default angular.module('directives.footerweb', [])
  .component('footerweb', {
    template: require('./footerweb.html'),
    controller: FooterWebComponent
  })
  .name;
