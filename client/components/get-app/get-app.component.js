import angular from 'angular';

export class GetAppComponent {}

export default angular.module('directives.getApp', [])
  .component('getApp', {
    template: require('./get-app.html'),
    controller: GetAppComponent
  })
  .name;
