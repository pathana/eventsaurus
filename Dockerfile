# Compose:
# docker-compose up -d

FROM ubuntu:latest

RUN apt-get update -q \
  && apt-get install -yqq \
  curl \
  git \
  ssh \
  gcc \
  make \
  build-essential \
  libkrb5-dev \
  libssl-dev \
  sudo \
  apt-utils \
  && apt-get clean \
  && rm -rf /var/lib/aptlists/* /tmp/* /var/tmp/*

RUN curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh -o install_nvm.sh
RUN bash install_nvm.sh

RUN node

RUN npm
